package com.desafio.api.v1.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.desafio.api.v1.dto.DigitoUnicoDTO;
import com.desafio.api.v1.dto.UsuarioDTO;
import com.desafio.service.DigitoUnicoService;
import com.desafio.service.UsuarioService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@WebMvcTest(UsuarioController.class)
public class UsuarioControllerTest {

	private MockMvc mockMvc;

	@Autowired
	private UsuarioController usuarioController;

	@MockBean
	private UsuarioService usuarioService;

	@MockBean
	private DigitoUnicoService digitoUnicoService;

	@Before
	public void setUp() {
		Mockito.reset(usuarioService);
		mockMvc = MockMvcBuilders.standaloneSetup(usuarioController).build();
	}

	@Test
	public void testAddUser() throws JsonProcessingException, Exception {
		UsuarioDTO dto = new UsuarioDTO(1l, "Ricardo Murta", "ricardomurta.rms@gmail.com", new ArrayList<DigitoUnicoDTO>());

		when(usuarioService.save(usuarioService.fromDTO(dto))).thenReturn(dto);

		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(post("/usuario/addUser").content(objectMapper.writeValueAsBytes(dto))
				.contentType(APPLICATION_JSON_UTF8)).andExpect(status().isOk()).andExpect(jsonPath("$.*", hasSize(4)));

		verify(usuarioService).save(usuarioService.fromDTO(dto));
	}

	@Test
	public void testFindById() throws JsonProcessingException, Exception {
		UsuarioDTO dto = new UsuarioDTO(1l, "Ricardo Murta", "ricardomurta.rms@gmail.com", new ArrayList<DigitoUnicoDTO>());

		when(usuarioService.findById(1l)).thenReturn(dto);

		mockMvc.perform(get("/usuario/findById/1")).andExpect(status().isOk()).andExpect(jsonPath("$.*", hasSize(4)));
	}

}
