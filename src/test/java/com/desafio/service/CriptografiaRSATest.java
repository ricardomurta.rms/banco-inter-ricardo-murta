package com.desafio.service;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CriptografiaRSATest {
	
	private static final String publicKeyString = "MIIBITANBgkqhkiG9w0BAQEFAAOCAQ4AMIIBCQKCAQBxY/C2jGi0WqhJzRuDb3WxTk+jOESvvogaviNMPz2H79SINiDL4nUllMc6uI9wu4TvRq21QIgzSG7/9xwQx5shZkUvG6g0DvmPLL33NkB/7wBuIWmxxHcRGbg0rSX7JFfq7BPm314qqajsvrIdCn+mY8yCzmCmHl9NC46ewaUcRUl8h/pVlgZhbZ9Ez2XuOR0D7pBSK2UQnx77UjnqJLj3ZvaonSAURE+1kAUPoB1frlGZ02D+nKsdpIpL6aWgiowwsD0yUbfuofToJfMzIpXfniyjA/MoJkcpcW9hhzeBKBDgglGV1Ie2fIJG/CMi/HkQC5JLzqd3IQUXddvcqwbTAgMBAAE=";

//	private static final String publicKeyStringInvalid = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsfrZFFGNWajr795iQcHO"
//			+ "oLStH2Ty1I9TyaW8EcskSFO/UfciyI3nHh7fpWSldGIXw6GHG+M8yhjzuXSB7vUd0FohwV2a3i1GVgXge/CpYKucBmKZQCCiewvblsvX"
//			+ "jSftTvGB3jfbaJw5wgsmJe9gjecvG/91Swq9rfHh6hBeM3lhrAzuyfkLUjB5z/RidNa3bRNvHvsQJ+NK9+uA7Gq91IlkoBBdJyfWIwow"
//			+ "aiQxKiSyd2eBh774Z74pqKS9Wd0aRY8SLpBYkCAaSKHEvOjrgzzfr3RSEmKR3kMf8FdcNrAAaZO8JKv2WtCAolN5iriP/QbpbZxJ8LTz"
//			+ "dxS6pxfVxQIDAQABk";

	@InjectMocks
	private CriptografiaRSA criptografiaRSA;

	@Test
	public void should_encrypt_correctly() {

		final String unencryptedValue = "valor não criptografado";
		final String encryptedValue = criptografiaRSA.encrypt(publicKeyString, unencryptedValue);

		assertThat(encryptedValue).isNotEmpty();
		assertThat(encryptedValue).isNotNull();
		assertThat(encryptedValue).isNotEqualTo(unencryptedValue);
	}


}
