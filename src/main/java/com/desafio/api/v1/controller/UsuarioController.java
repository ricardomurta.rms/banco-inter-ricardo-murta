package com.desafio.api.v1.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.desafio.api.v1.dto.DigitoUnicoDTO;
import com.desafio.api.v1.dto.UsuarioDTO;
import com.desafio.service.DigitoUnicoService;
import com.desafio.service.UsuarioService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Controller
@RequestMapping("usuario")
@Api(value = "API REST Usuários")
public class UsuarioController {
	
	private UsuarioService usuarioService;
	private DigitoUnicoService digitoUnicoService;
	
	public UsuarioController(UsuarioService usuarioService, DigitoUnicoService digitoUnicoService) {
		this.usuarioService = usuarioService;
		this.digitoUnicoService = digitoUnicoService;
	}

	@PostMapping("/addUser")
	@ApiOperation(value = "Método utilizado para adicionar usuários.")
	public ResponseEntity<UsuarioDTO> addUser(@RequestBody UsuarioDTO usuarioDTO) {
		return new ResponseEntity<UsuarioDTO>(usuarioService.save(usuarioService.fromDTO(usuarioDTO)), HttpStatus.OK);
	}
	
	@PutMapping("/addDigitoUnicoUsuario/{id}")
	@ApiOperation(value = "Método utilizado para adicionar dígito único a um determinado usuário.")
	public ResponseEntity<UsuarioDTO> addDigitoUnicoUsuario(@RequestBody DigitoUnicoDTO digitoUnicoDTO, @PathVariable("id") Long id) {
		return new ResponseEntity<UsuarioDTO>(usuarioService.save(digitoUnicoService.fromDigitoUnicoDTO(digitoUnicoDTO), id), HttpStatus.OK);
	}
	
	@DeleteMapping("/delete/{id}")
	@ApiOperation(value = "Método utilizado para deletar usuário.")
	public ResponseEntity<Void> deleteById(@PathVariable("id") Long id) {
		usuarioService.delete(id);
		return ResponseEntity.noContent().build();
	}
	
	@GetMapping("/findById/{id}")
	@ApiOperation(value = "Método utilizado para encontrar usuário de acordo com o id.")
	public ResponseEntity<UsuarioDTO> findById(@PathVariable("id") Long id) {
		return new ResponseEntity<UsuarioDTO>(usuarioService.findById(id), HttpStatus.OK);
	}
	
	@PutMapping("/encrypt/{id}")
	@ApiOperation(value = "Método utilizado para criptografar os dados de um determinado usuário.")
	public ResponseEntity<Void> encryptUser(@RequestBody String publicKey, @PathVariable("id") Long id){
		usuarioService.encrypt(id, publicKey);
		return ResponseEntity.noContent().build();
	}

}
