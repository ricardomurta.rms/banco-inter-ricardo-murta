package com.desafio.api.v1.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.desafio.domain.DigitoUnico;
import com.desafio.service.DigitoUnicoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Controller
@RequestMapping("digitounico")
@Api(value = "API REST Digito Unico")
public class DigitoUnicoController {
	
	private DigitoUnicoService digitoUnicoService;
	
	public DigitoUnicoController(DigitoUnicoService digitoUnicoService) {
		this.digitoUnicoService = digitoUnicoService;
	}

	@PostMapping("/addDigitoUnico")
	@ApiOperation(value = "Método utilizado para adicionar um dígito único não atrelado a um usuário.")
	public ResponseEntity<DigitoUnico> addDigitoUnico(@RequestBody DigitoUnico digitoUnico) {
		return new ResponseEntity<DigitoUnico>(digitoUnicoService.save(digitoUnico), HttpStatus.OK);
	}
	
}
