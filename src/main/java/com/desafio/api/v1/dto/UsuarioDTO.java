package com.desafio.api.v1.dto;

import java.util.ArrayList;
import java.util.List;

public class UsuarioDTO {
	
	private Long id;
	private String nome;
	private String email;
	private List<DigitoUnicoDTO> lista = new ArrayList<DigitoUnicoDTO>();
	
	public UsuarioDTO() {}
	
	public UsuarioDTO(Long id, String nome, String email, List<DigitoUnicoDTO> lista) {
		this.id = id;
		this.nome = nome;
		this.email = email;
		this.lista = lista;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<DigitoUnicoDTO> getLista() {
		return lista;
	}
	public void setLista(List<DigitoUnicoDTO> lista) {
		this.lista = lista;
	}

}
