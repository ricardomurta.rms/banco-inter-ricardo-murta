package com.desafio.api.v1.dto;

import java.math.BigInteger;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class DigitoUnicoDTO {

	private Long id;
	private BigInteger digitoUnico;
	private BigInteger n;
	private Integer k;
	@JsonIgnore
	private UsuarioDTO usuario;

	public DigitoUnicoDTO() {
	}

	public DigitoUnicoDTO(BigInteger n, Integer k) {
		this.n = n;
		this.k = k;
	}
	
	
	

	public DigitoUnicoDTO(Long id, BigInteger digitoUnico, BigInteger n, Integer k) {
		super();
		this.id = id;
		this.digitoUnico = digitoUnico;
		this.n = n;
		this.k = k;
	}

	/*
	 * Getters & Setters
	 */
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public BigInteger getDigitoUnico() {
		return digitoUnico;
	}

	public void setDigitoUnico(BigInteger digitoUnico) {
		this.digitoUnico = digitoUnico;
	}

	public BigInteger getN() {
		return n;
	}

	public void setN(BigInteger n) {
		this.n = n;
	}

	public Integer getK() {
		return k;
	}

	public void setK(Integer k) {
		this.k = k;
	}

	public UsuarioDTO getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioDTO usuario) {
		this.usuario = usuario;
	}

}
