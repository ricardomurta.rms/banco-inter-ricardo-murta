package com.desafio.service;

import java.math.BigInteger;

import org.springframework.stereotype.Service;

import com.desafio.api.v1.dto.DigitoUnicoDTO;
import com.desafio.domain.Cache;
import com.desafio.domain.DigitoUnico;
import com.desafio.repository.DigitoUnicoRepo;

@Service
public class DigitoUnicoService {
	
	private DigitoUnicoRepo digitoUnicoRepo;
	private Cache cache;

	public DigitoUnicoService(DigitoUnicoRepo digitoUnicoRepo, Cache cache) {
		this.digitoUnicoRepo = digitoUnicoRepo;
		this.cache = cache;
	}
	
	public DigitoUnico save(DigitoUnico du) {
		
		DigitoUnico duCache = cache.find(du);
		if(duCache != null) {
			du.setDigitoUnico(duCache.getDigitoUnico());
			du.setK(duCache.getK());
			du.setN(duCache.getN());
			du = digitoUnicoRepo.save(du);
		}else {
			du = digitoUnicoRepo.save(du.calcularDigitoUnico());
			cache.addCache(du);
		}
		return du;
		
	}
	
	public void delete(String n, Integer k) {
		DigitoUnico du = digitoUnicoRepo.findByNAndK(new BigInteger(n), k);
		digitoUnicoRepo.delete(du);
	}
	
	public DigitoUnico fromDigitoUnicoDTO(DigitoUnicoDTO dto) {
		return new DigitoUnico(dto.getId(), dto.getDigitoUnico(), dto.getN(), dto.getK());
	}
	
	

}
