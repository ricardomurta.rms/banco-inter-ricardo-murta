package com.desafio.service;

import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import javax.crypto.Cipher;

import org.hibernate.service.spi.ServiceException;
import org.springframework.stereotype.Component;

@Component
public class CriptografiaRSA {
	
	private static final String ALGORITMO_DE_CRIPTOGRAFIA = "RSA";
	  
	  private PublicKey generatePublicKey(String encryptionKey) throws Exception {
	    
	    byte[] encryptionKeyBytes = Base64.getDecoder().decode(encryptionKey);
	    X509EncodedKeySpec publicSpec = new X509EncodedKeySpec(encryptionKeyBytes);
	    KeyFactory keyFactory = KeyFactory.getInstance(ALGORITMO_DE_CRIPTOGRAFIA);
	    PublicKey publicKey = keyFactory.generatePublic(publicSpec);
	    
	    return publicKey;
	  }
	  
	  public String encrypt(String encryptionKey, String value) {
	    
	    byte[] cipherText = null;

	    try {
	      PublicKey publicKey = generatePublicKey(encryptionKey);
	      Cipher cipher = Cipher.getInstance(ALGORITMO_DE_CRIPTOGRAFIA);
	      cipher.init(Cipher.ENCRYPT_MODE, publicKey);
	      cipherText = cipher.doFinal(value.getBytes());
	    
	    } catch (Exception e) {
	      throw new ServiceException("Error de criptografia", e);
	    }
	    return new String(cipherText);
	  }


}
