package com.desafio.service.exceptions;

public class CannotCalculateException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public CannotCalculateException(String msg) {
		super(msg);
	}
	
	public CannotCalculateException(String msg, Throwable cause) {
		super(msg, cause);
	}

}