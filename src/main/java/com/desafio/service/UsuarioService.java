package com.desafio.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.desafio.api.v1.dto.DigitoUnicoDTO;
import com.desafio.api.v1.dto.UsuarioDTO;
import com.desafio.domain.Cache;
import com.desafio.domain.DigitoUnico;
import com.desafio.domain.Usuario;
import com.desafio.repository.UsuarioRepo;
import com.desafio.service.exceptions.CannotCalculateException;
import com.desafio.service.exceptions.UserNotFoundException;

@Service
public class UsuarioService {
	
	private UsuarioRepo usuarioRepo;
	private Cache cache;
	private CriptografiaRSA criptografiaRSA;

	public UsuarioService(Cache cache, UsuarioRepo usuarioRepo, CriptografiaRSA criptografiaRSA) {
		this.cache = cache;
		this.usuarioRepo = usuarioRepo;
		this.criptografiaRSA = criptografiaRSA;
	}
	
	public UsuarioDTO save(Usuario usuario) {
		return fromUsuario(usuarioRepo.save(usuario));
	}
	
	public UsuarioDTO save(DigitoUnico du, Long id) {
		
		Usuario usuario = usuarioRepo.findById(id).orElseThrow(
				() -> new UserNotFoundException("Usuário não cadastrado. É necessário cadastrar antes de solicitar cálculos"));
		du.setUsuario(usuario);
		
		DigitoUnico duCache = cache.find(du);
		if(duCache != null) {
			du.setDigitoUnico(duCache.getDigitoUnico());
			usuario.getLista().add(du);
			usuarioRepo.save(usuario);
		}else {
			try {
				du.calcularDigitoUnico();
			} catch (CannotCalculateException e) {
				throw(new CannotCalculateException("Não foi possível calcular. Os parâmetros informados não estão de acordo com as regras."));
			}
			usuario.getLista().add(du);
			usuarioRepo.save(usuario);
			cache.addCache(du);
		}
		return fromUsuario(usuario);
	}
	
	public void delete(Long id) {
		Usuario usuario = usuarioRepo.findById(id).orElseThrow(() -> new UserNotFoundException("O id informado é inválido."));
		usuarioRepo.delete(usuario);
	}
	
	public UsuarioDTO findById(Long id) {
		return fromUsuario(usuarioRepo.findById(id).orElseThrow(() -> new UserNotFoundException("O id informado é inválido.")));
	}
	
	public void encrypt(Long id, String publicKey) {
		Usuario usuario = usuarioRepo.findById(id).orElseThrow(() -> new UserNotFoundException("O id informado é inválido."));
		usuario.setPublicKey(publicKey);
		usuario.setNome(criptografiaRSA.encrypt(publicKey, usuario.getNome()));
		usuario.setEmail(criptografiaRSA.encrypt(publicKey, usuario.getEmail()));
		usuarioRepo.save(usuario);
	}
	
	public Usuario fromDTO(UsuarioDTO usuarioDTO) {
		List<DigitoUnico> listaDU = new ArrayList<DigitoUnico>();
		if(usuarioDTO.getLista() != null) {
			for (DigitoUnicoDTO dto : usuarioDTO.getLista()) {
				DigitoUnico du = new DigitoUnico(dto.getId(), dto.getDigitoUnico(), dto.getN(), dto.getK());
				listaDU.add(du);
			}
		}
		return new Usuario(usuarioDTO.getId(), usuarioDTO.getNome(), usuarioDTO.getEmail(), listaDU);
	}
	
	public UsuarioDTO fromUsuario(Usuario usuario) {
		List<DigitoUnicoDTO> listaDUDTO = new ArrayList<DigitoUnicoDTO>();
		for (DigitoUnico du : usuario.getLista()) {
			DigitoUnicoDTO dto = new DigitoUnicoDTO(du.getId(), du.getDigitoUnico(), du.getN(), du.getK());
			listaDUDTO.add(dto);
		}
		return new UsuarioDTO(usuario.getId(), usuario.getNome(), usuario.getEmail(), listaDUDTO);
	}
	
}
