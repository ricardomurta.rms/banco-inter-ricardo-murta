package com.desafio.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.desafio.domain.Usuario;

public interface UsuarioRepo extends JpaRepository<Usuario, Long> {
	
}
