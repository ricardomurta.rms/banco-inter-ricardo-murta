package com.desafio.repository;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.desafio.domain.DigitoUnico;

public interface DigitoUnicoRepo extends JpaRepository<DigitoUnico, Long> {
	
	@Query("SELECT du FROM DigitoUnico du WHERE du.n = :n and du.k = :k")
	DigitoUnico findByNAndK(@Param("n") BigInteger n, @Param("k") Integer k);

}
