package com.desafio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DesafioBancoInter2Application {

	public static void main(String[] args) {
		SpringApplication.run(DesafioBancoInter2Application.class, args);
	}

}
