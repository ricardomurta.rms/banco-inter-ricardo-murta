package com.desafio.domain;

import java.math.BigInteger;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.desafio.service.exceptions.CannotCalculateException;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class DigitoUnico {

	private static final BigInteger MAX_N = new BigInteger("10").pow(1000000);
	private static final Integer MAX_K = (int) Math.pow(10, 5);

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private BigInteger digitoUnico;
	private BigInteger n;
	private Integer k;
	@ManyToOne @JsonIgnore
	private Usuario usuario;

	public DigitoUnico() {
	}

	public DigitoUnico(BigInteger n, Integer k) {
		this.n = n;
		this.k = k;
	}
	
	public DigitoUnico(BigInteger n, Integer k, Usuario usuario) {
		this.n = n;
		this.k = k;
		this.usuario = usuario;
	}
	
	public DigitoUnico(Long id, BigInteger digitoUnico, BigInteger n, Integer k) {
		this.id = id;
		this.digitoUnico = digitoUnico;
		this.n = n;
		this.k = k;
	}

	public DigitoUnico calcularDigitoUnico() {
		if(!validate()) {
			throw new CannotCalculateException("Validação de parâmetros.");
		}else {
			this.digitoUnico = CalculaDigitoUnico.digitoUnico(n, k);
			return this;
		}
	}

	private Boolean validate() {
		if (n.compareTo(new BigInteger("1")) < 0 || n.compareTo(MAX_N) > 0 || k < 1 || k > MAX_K)
			return false;
		else
			return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((k == null) ? 0 : k.hashCode());
		result = prime * result + ((n == null) ? 0 : n.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DigitoUnico other = (DigitoUnico) obj;
		if (k == null) {
			if (other.k != null)
				return false;
		} else if (!k.equals(other.k))
			return false;
		if (n == null) {
			if (other.n != null)
				return false;
		} else if (!n.equals(other.n))
			return false;
		return true;
	}

	/*
	 * Getters & Setters
	 */
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public BigInteger getDigitoUnico() {
		return digitoUnico;
	}

	public void setDigitoUnico(BigInteger digitoUnico) {
		this.digitoUnico = digitoUnico;
	}

	public BigInteger getN() {
		return n;
	}

	public void setN(BigInteger n) {
		this.n = n;
	}

	public Integer getK() {
		return k;
	}

	public void setK(Integer k) {
		this.k = k;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
}
