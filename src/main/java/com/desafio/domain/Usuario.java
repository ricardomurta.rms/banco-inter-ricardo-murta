package com.desafio.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;

@Entity
public class Usuario {
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Lob
	private String nome;
	@Lob
	private String email;
	@OneToMany(mappedBy = "usuario", cascade = CascadeType.ALL)
	private List<DigitoUnico> lista = new ArrayList<DigitoUnico>();
	@Lob
	private String publicKey;
	
	public Usuario() {
	}
	
	public Usuario(String nome, String email) {
		this.nome = nome;
		this.email = email;
	}
	
	public Usuario(Long id, String nome, String email) {
		this.id = id;
		this.nome = nome;
		this.email = email;
	}
	
	public Usuario(Long id, String nome, String email, List<DigitoUnico> lista) {
		this.id = id;
		this.nome = nome;
		this.email = email;
		this.lista = lista;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<DigitoUnico> getLista() {
		return lista;
	}
	public void setLista(List<DigitoUnico> lista) {
		this.lista = lista;
	}

	public String getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}
	
	

}
