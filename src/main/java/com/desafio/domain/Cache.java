package com.desafio.domain;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("application")
public class Cache {
	
	private static final Integer CACHE_MAX_SIZE = 10;
	
	private List<DigitoUnico> cacheDigitosCalculados = new ArrayList<DigitoUnico>();
	
	public void addCache(DigitoUnico du) {
		if(cacheDigitosCalculados.size() == CACHE_MAX_SIZE) {
			cacheDigitosCalculados.remove(0);
		}
		cacheDigitosCalculados.add(du);
	}
	
	public DigitoUnico find(DigitoUnico du) {
		DigitoUnico di;
		if(cacheDigitosCalculados.contains(du)) {
			di = cacheDigitosCalculados.get(cacheDigitosCalculados.indexOf(du));
			System.out.println(di.getN() + " - " + di.getK() + " - " + di.getDigitoUnico());
			return di;
		}else {
			return null;
		}
	}

}
