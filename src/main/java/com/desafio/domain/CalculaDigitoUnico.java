package com.desafio.domain;

import java.math.BigInteger;

public class CalculaDigitoUnico {

	public static BigInteger digitoUnico(BigInteger n, Integer k) {
		String strN = "";
		for(Integer i = 0; i < k; i++) {
			strN = strN + String.valueOf(n);
		}
		BigInteger bi = new BigInteger(strN);
		return generateDigitoUnico(bi);
	}

	private static BigInteger generateDigitoUnico(BigInteger n) {
		while (n.compareTo(new BigInteger("10")) >= 0) {
			n = calculate(n);
		}
		return n;
	}

	private static BigInteger calculate(BigInteger n) {
		BigInteger aux = n;
		BigInteger result = new BigInteger("0");
		while (aux.compareTo(new BigInteger("0")) > 0) {
			result = result.add(aux.mod(new BigInteger("10")));
			aux = aux.divide(new BigInteger("10"));
		}
		return result;
	}
	
}
