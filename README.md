# Banco Inter - Ricardo Murta dos Santos

Implementação do Desafio Java do Banco Inter.

## Building

1. Execute a compilação completa do Maven para criar o arquivo `target/DesafioBancoInter-2-0.0.1-SNAPSHOT.jar`:
    ```bash
    $ mvn clean install
    ```
## Tests

2. Execute os testes da aplicação a partir da raiz do projeto com o comando:
    ```bash
    $ mvn test
    ```

## Run

3. Rode a aplicação a partir do diretório `target/` com o comando:
    ```bash
    $ java -jar DesafioBancoInter-2-0.0.1-SNAPSHOT.jar
    ```

## Documentação da API

4. A documetação da api pode ser vista em: http://localhost:8080/swagger-ui.html